﻿using System;
using TechTalk.SpecFlow;

namespace Facebook
{
    [Binding]
    public class SpecFlowFeature1Steps
    {
        [Given(@"the first number is '(.*)'")]
        public void GivenTheFirstNumberIs(string p0, Table table)
        {
            ScenarioContext.Current["first number"] = p0;
            var temp = (string)ScenarioContext.Current["first number"];

            FeatureContext.Current["name"] = "Nastya";
            var name = (string)FeatureContext.Current["name"];

            ScenarioContext.Current.Pending();
        }
    }
}
