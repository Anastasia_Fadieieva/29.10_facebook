﻿using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Facebook
{
    [Binding]
    public class Class1
    {
        string defaultPassword = "123qwe";
        string defaultMail = "123qwe@gmail.com";
        Dictionary<string, string> uniqUser = new Dictionary<string, string>() { { "Alina", "pweoiw98r03" } };

        [StepArgumentTransformation(@"default (\w+)")]
        public string GetDefPassword(string value)
        {
            var result = "";

            if (value.Equals("password"))
                result = defaultPassword;
            else if (value.Equals("mail"))
                result = defaultMail;

            return result;
        }


        [StepArgumentTransformation]
        public string GetDefPassword(Table table)
        {
            return defaultPassword;
        }
    }
}
