﻿
Feature: Registration
	In order to chat with friends
	As a user of the application Facebook
	I want to be able to register in this application

@p1 @smoke
Scenario Outline: Check that user can register with valid data on the Registration form
	Given Registration page is open
	When I fill Registration form fields with valid data
	| FirstName    | LastName    | NewPassword    | Month   | Day   | Year   | Gender   |
	| <first name> | <last name> | <new password> | <month> | <day> | <year> | <gender> |
	And I fill Email field
	And I click Sign up button
	#And I enter Conformation code
	Then The message appears: 'We Need More Information'
	#Then User's personal account created
	#And User on the home page
Examples: 
	| first name | last name | new password | month | day | year | gender |
	| Petya      | Ivanov    | Qwerty321    | Sep   | 16  | 1991 | Male   |
	| Lucas      | Starwar   | 123987Aa     | Feb   | 11  | 1999 | Male   |
	| Cristiana  | Grey      | tr2&Rt842    | Sep   | 10  | 2000 | Female |


@p1 
Scenario Outline: Check that user can register with valid data on the Registration form for Custom gender
	Given Registration page is open
	When I fill Registration form fields with valid data
	| FirstName    | LastName    | NewPassword    | Month   | Day   | Year   | Gender   | SelectPronoun    | GenderOptional    |
	| <first name> | <last name> | <new password> | <month> | <day> | <year> | <gender> | <select pronoun> | <gender optional> |
	And I fill Email field
	And I click Sign up button
	#And I enter Conformation code
	#Then User's personal account created
	Then The message appears: 'We Need More Information'
	#And User on the home page
Examples:
	| first name | last name | new password      | month | day | year | gender | select pronoun                      | gender optional |
	| Lev        | Tolstoy   | 575178Ll          | Nov   | 28  | 2009 | Custom | He:"Whish him a happy birthday!"    | null            |
	| Adam       | Gontier   | gusldghl!!bhds123 | Dec   | 20  | 1984 | Custom | She:"Whish her a happy birthday!"   | Rock'n'roll     |
	| Lola       | Milkovich | hhsuguwg?b fdsf   | Oct   | 01  | 1973 | Custom | They:"Whish them a happy birthday!" | Qween of Shadow | 

@p1 
Scenario Outline: Check that user cant register with invalid data on the Registration form
	Given Registration page is open
	When I fill Registration form fields with valid data
	| FirstName    | LastName    | NewPassword    | Month   | Day   | Year   | Gender   |
	| <first name> | <last name> | <new password> | <month> | <day> | <year> | <gender> |
	And I fill Email field
	And I click Sign up button
	Then Then The notification '<notification>' is displayed
Examples: 
	| first name | last name | new password | month | day | year | gender | notification                                                                             |
	| Petya      | Ivanov    | Qwert        | Sep   | 16  | 1991 | Male   | Your password must be at least 6 characters long. Please try another.                    |
	| Tarantino  | Milano    | 123987dff    | Fab   | 11  | 2010 | Male   | We Couldn't Create Your Account We were not able to sign you up for Facebook             |
	| null       | null      | tr2&Rt84     | Sep   | 10  | 2000 | Female | What's your name?                                                                        |
	| Tilla      | Grey      | null         | Oct   | 10  | 2005 | Female | You'll use this when you log in and if you ever need to reset your password.             |
	| Petya      | /         | 2398fs7dff   | Nov   | 25  | 1967 | Male   | This name has certain characters that aren't allowed. Learn more about our name policies |
	| Tilla      | Tekila    | 2398d7dff    | Oct   | 10  | 2005 | null   | Please choose a gender. You can change who can see this later.                           |


	