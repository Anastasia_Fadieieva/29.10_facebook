﻿Feature: Authorization
	In order to use the available functions of the web application
	As a user of the Facebook web application
	I want to be able to log in 

Background:
	Given The page of authorization is open

@p1 @smoke
Scenario Outline: Authorization with valid data
	When I fill Autorization form fields with Email and Password
		| Email   | Password   |
		| <email> | <password> |
	And I click on Log In button
	Then The user's personal account page is open
Examples:
		| email                | password    |
		| zimleyirte@vusra.com | Koly911_Yes |

@p1 @smoke
Scenario: Check that when the user clicks on the button Create New Account the popup registration form is displayed to the user
	When I click on the button Create New Account
	Then The user sees the registration popup

@p1
Scenario: Make sure that when the user clicks the Forgot Password? button, the user is taken to the account search page
	When I click on the button Forgot Password?
	Then The user went to the account search page

@p1
Scenario: Check that an unregistered user cannot log in
	When I fill Authorization field
		| Login                 | Password  |
		| recotos706@dukeoo.com | Qwerty123 |
	And I click on Log In button
	Then The user sees the message Wrong Credentials Invalid username or password on the display
	And User stay on Autorization page

@p2
Scenario: Check that when entering an invalid email and a valid password, the user sees error message
	When I fill Autorization form fields with valid data
		| Email               | Password    |
		| zimleyirte@vusra.co | Koly911_Yes |
	And I click on Log In button
	Then The user sees the message Wrong Credentials Invalid username or password on the display
	And User stay on Autorization page

@p2
Scenario: Check that when entering an valid email and a invalid password, the user sees error messages
	When I fill Autorization form fields with valid data
		| Email                | Password       |
		| zimleyirte@vusra.com | Koly911_Ye878s |
	And I click on Log In button
	Then The user sees the message Wrong Credentials Invalid username or password on the display
	And User stay on Autorization page