﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facebook.UI.Models
{
    public class RegistrationModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NewPassword { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
        public int Year { get; set; }
        public string Gender { get; set; }
        public string SelectPronoun { get; internal set; }
        public string GenderOptional { get; internal set; }
    }
}
