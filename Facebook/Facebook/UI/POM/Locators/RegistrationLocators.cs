﻿using OpenQA.Selenium;

namespace Facebook.UI.POM.Locators
{
    public class RegistrationLocators
    {
        public static int _num { get; set; }
        public int _day { get; set; }

        internal By _birthdayDayValue;
        internal By _birthdayMonthValue;
        internal By _birthdayYearValue;

        internal By _preferredPronoun;
        public RegistrationLocators(int day, int month, int year)
        {
            _birthdayDayValue = By.XPath($"//*[@id='day']/option[{day}]");
            _birthdayMonthValue = By.XPath($"//*[@id='day']/option[{month}]");
            _birthdayYearValue = By.XPath($"//*[@id='day']/option[{year}]");

        }
        public RegistrationLocators(int pronounNum)
        {
            _preferredPronoun = By.XPath($"/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[8]/div[1]/select/option[{pronounNum}]");
        }

        internal static By _language2 = By.XPath("//*[@id='pageFooter']/ul/li[2]/a");

        internal static By _language3 = By.XPath("//*[@id='pageFooter']/ul/li[3]/a");

        internal static By _footerPage = By.XPath("//*[@id='pageFooter']/ul");

        internal static By _language = By.XPath("//*[@id='pageFooter']/ul/li[1]");

        internal static By _createNewAccountButton = By.XPath("/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[5]/a");

        internal static By _firstNameInputButton = By.Name("firstname");

        internal static By _lastNameInputButton = By.Name("lastname");

        internal static By _emailInputButton = By.Name("reg_email__");

        internal static By _reEnterEmailInputButton = By.Name("reg_email_confirmation__");

        internal static By _passwordInputButton = By.Id("password_step_input");

        internal static By _birthdayMonthDropDown = By.Name("birthday_month");

        internal static By _birthdayDayDropDown = By.Name("birthday_day");

        internal static By _birthdayYearDropDown = By.Name("birthday_year");

        internal static By _maleGenderButton = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[7]/span/span[2]/label");

        internal static By _femaleGenderButton = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[7]/span/span[1]/label");

        internal static By _customGenderButton = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[7]/span/span[3]/label");

        internal static By _fieldPreferredPronoun = By.Name("preferred_pronoun");

        //internal static By _preferredPronoun = By.XPath($"/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[8]/div[1]/select/option[{_num}]");

        internal static By _genderOptional = By.XPath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/div[1]/form/div[1]/div[8]/div[3]/div/input");

        internal static By _submitButton = By.Name("websubmit");

        internal static By _codeConfirmInputButton = By.Name("code");

        internal static By _Ok = By.XPath("//*[@id='facebook']/body/div[4]/div[2]/div/div/div/div[3]/div/a");

        internal static By _AccountName = By.CssSelector("#mount_0_0_bM > div > div:nth-child(1) > div > div.rq0escxv.l9j0dhe7.du4w35lb > div > div > div.j83agx80.cbu4d94t.d6urw2fd.dp1hu0rb.l9j0dhe7.du4w35lb > div.rq0escxv.l9j0dhe7.du4w35lb.j83agx80.pfnyh3mw.taijpn5t.gs1a9yip.owycx6da.btwxx1t3.dp1hu0rb.p01isnhg > div > div.rq0escxv.lpgh02oy.du4w35lb.o387gat7.qbu88020.pad24vr5.rirtxc74.dp1hu0rb.fer614ym.ni8dbmo4.stjgntxs.rek2kq2y.be9z9djy.bx45vsiw > div > div > div.j83agx80.cbu4d94t.buofh1pr.l9j0dhe7 > div > div > div.buofh1pr > ul > li > div > a > div.ow4ym5g4.auili1gw.rq0escxv.j83agx80.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.nnctdnn4.hpfvmrgz.qt6c0cv9.jb3vyjys.l9j0dhe7.du4w35lb.bp9cbjyn.btwxx1t3.dflh9lhu.scb9dxdr > div.ow4ym5g4.auili1gw.rq0escxv.j83agx80.buofh1pr.g5gj957u.i1fnvgqd.oygrvhab.cxmmr5t8.hcukyx3x.kvgmc6g5.tgvbjcpo.hpfvmrgz.qt6c0cv9.rz4wbd8a.a8nywdso.jb3vyjys.du4w35lb.bp9cbjyn.btwxx1t3.l9j0dhe7 > div > div > div > div > span > span");

        internal static By _customGenderInputButton = By.Name("custom_gender");

        internal static By _WeNeedMoreInformation = By.XPath("//*[@id='content']/div/div[1]/div/div[1]/div/div/div/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div[1]/span");

        internal static By _errorMesage = By.XPath("//*[@id='reg_error_inner']");
    }
}
