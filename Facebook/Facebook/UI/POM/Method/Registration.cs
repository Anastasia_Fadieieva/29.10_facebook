﻿using Facebook.Support;
using Facebook.TempMail;
using Facebook.UI.POM.Locators;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facebook.UI.POM.Method
{
    internal class Registration
    {
        internal static string GetLanguage() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._language).Text;
        internal static string GetLanguage2() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._language2).Text;
        internal static void ClickLanguage2() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._language2).Click();
        internal static string GetLanguage3() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._language3).Text;
        internal static void ClickLanguage3() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._language3).Click();
        internal static string GetListLanguages() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._footerPage).Text;
        internal static void ClickRegistartionButton() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._createNewAccountButton).Click();
        internal static void SetFirstName(string firstname) =>
            ChromeBrowser.GetDriver().FindElement(RegistrationLocators._firstNameInputButton).SendKeys(firstname);
        internal static void SetLastName(string lasttname) =>
            ChromeBrowser.GetDriver().FindElement(RegistrationLocators._lastNameInputButton).SendKeys(lasttname);
        internal static void SetEmail(string email) =>
            ChromeBrowser.GetDriver().FindElement(RegistrationLocators._emailInputButton).SendKeys(email)
;
        internal static void SetReEmail(string email) =>
            ChromeBrowser.GetDriver().FindElement(RegistrationLocators._reEnterEmailInputButton).SendKeys(email)
;
        internal static void SetPassword(string password) =>
            ChromeBrowser.GetDriver().FindElement(RegistrationLocators._passwordInputButton).SendKeys(password);

        internal static void SetAndClickAllDate(int day, string month, int year)
        {
            IWebElement elementDay = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayDayDropDown);
            SelectElement selectDay = new SelectElement(elementDay);
            selectDay.SelectByValue(day.ToString());

            IWebElement elementMonth = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayMonthDropDown);
            SelectElement selectMonth = new SelectElement(elementMonth);
            selectMonth.SelectByText(month);

            IWebElement elementYear = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayYearDropDown);
            SelectElement selectYear = new SelectElement(elementYear);
            selectYear.SelectByValue(year.ToString());
        }
        internal static void SetAndClickAllDateAndGender(int day, string month, int year, string gender)
        {
            IWebElement elementDay = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayDayDropDown);
            SelectElement selectDay = new SelectElement(elementDay);
            selectDay.SelectByValue(day.ToString());

            IWebElement elementMonth = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayMonthDropDown);
            SelectElement selectMonth = new SelectElement(elementMonth);
            selectMonth.SelectByText(month);

            IWebElement elementYear = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayYearDropDown);
            SelectElement selectYear = new SelectElement(elementYear);
            selectYear.SelectByValue(year.ToString());

            IWebElement elementGender = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._genderOptional);
            Registration.ClickFieldGenderOptional();
            SelectElement selectGender = new SelectElement(elementGender);
            selectGender.SelectByValue(gender.ToString());
        }
        internal static void SetAndClickBirthdayDay(int day)
        {
            IWebElement elementDay = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayDayDropDown);
            SelectElement selectDay = new SelectElement(elementDay);
            selectDay.SelectByValue(day.ToString());
        }
        internal static void SetAndClickBirthdayMonth(string month)
        {
            IWebElement elementMonth = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayMonthDropDown);
            SelectElement selectMonth = new SelectElement(elementMonth);
            selectMonth.SelectByText(month);
        }
        internal static void SetAndClickBirthdayYear(int year)
        {
            IWebElement elementYear = ChromeBrowser.GetDriver().FindElement(RegistrationLocators._birthdayYearDropDown);
            SelectElement selectYear = new SelectElement(elementYear);
            selectYear.SelectByValue(year.ToString());
        }
        internal static void ClickMaleGender() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._maleGenderButton).Click();
        internal static void ClickFemaleGender() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._femaleGenderButton).Click();
        internal static void ClickCustomGender() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._customGenderButton).Click();
        internal static void ClickFieldPreferredPronoun() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._fieldPreferredPronoun).Click();
        internal static void ClickFieldGenderOptional() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._genderOptional).Click();
        /// <summary>
        /// Этот метот выбирает из выпадающего списка желаемое местоимение
        /// </summary>
        /// <param name="pronoun">берем первую букву строки (строка[0])</param>
        internal static void SetPreferredPronoun(char pronoun)
        {
            RegistrationLocators locators;
            switch (pronoun)
            {
                case 'H':
                    locators = new RegistrationLocators(2);
                    ChromeBrowser.GetDriver().FindElement(locators._preferredPronoun).Click();
                    break;
                case 'S':
                    locators = new RegistrationLocators(3);
                    ChromeBrowser.GetDriver().FindElement(locators._preferredPronoun).Click();
                    break;
                case 'T':
                    locators = new RegistrationLocators(4);
                    ChromeBrowser.GetDriver().FindElement(locators._preferredPronoun).Click();
                    break;
            }            
        }
        internal static void ClickSignUpButton() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._submitButton).Click();
        internal static void SetCode(Mail mail)
        {
            MessageTempMail message = new MessageTempMail();
            var code = message.GetMessage(mail);
            ChromeBrowser.GetDriver().FindElement(RegistrationLocators._codeConfirmInputButton).SendKeys(code);
        }
        internal static bool ClickOk()
        {
            try
            {
                ChromeBrowser.GetDriver().FindElement(RegistrationLocators._Ok);
            }
            catch
            {
                return false;
            }
            return true;
        }

        internal static string GetAccountName() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._AccountName).Text;
        internal static string GetTittleWeNeedMoreInformation() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._WeNeedMoreInformation).Text;
        internal static string GetTextError() => ChromeBrowser.GetDriver().FindElement(RegistrationLocators._errorMesage).Text;
    }
}
