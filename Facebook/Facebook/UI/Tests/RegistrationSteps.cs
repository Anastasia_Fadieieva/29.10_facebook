﻿using Facebook.Support;
using Facebook.TempMail;
using Facebook.UI.Models;
using Facebook.UI.POM.Method;
using Facebook.UI.Tests.Methods;
using NUnit.Framework;
using System;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Facebook.Tests
{
    [Binding]
    public class RegistrationSteps : Hooks
    {
        RegistrationMethods methods = new RegistrationMethods();
        private string _tempMail { get; set; }
        private string _firstName { get; set; }
        private string _lastName { get; set; }

        [Given(@"Registration page is open")]
        public void GivenRegistrationPageIsOpen()
        {
            _tempMail = methods.CreateTempMail();
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://www.facebook.com/");
            RegistrationMethods.ChangeLang();
            Registration.ClickRegistartionButton();
        }

        [When(@"I fill Registration form fields with valid data")]
        public void WhenIFillRegistrationFormFieldsWithValidData(Table table)
        {
            var tableModel = table.CreateInstance<RegistrationModel>();
            _firstName = tableModel.FirstName;
            _lastName = tableModel.LastName;
            var newPass = tableModel.NewPassword;
            var day = tableModel.Day;
            var month = tableModel.Month;
            var year = tableModel.Year;
            var gender = tableModel.Gender;
            if (gender.Equals("Custom"))
            {
                var selectPronoun = tableModel.SelectPronoun;
                var genderOptional = tableModel.GenderOptional;
                RegistrationMethods.FillInTheRegistrationFieldsWithMyGender(_firstName, _lastName, newPass, day, month, year, selectPronoun, genderOptional); 
            }
            else
                RegistrationMethods.FillInTheRegistrationFieldsWithoutEmail(_firstName, _lastName, newPass, day, month, year, gender);
        }

        [When(@"I fill Email field")]
        public void WhenIFillEmailField()
        {
            Registration.SetEmail(_tempMail);
            Registration.SetReEmail(_tempMail);
        }

        [When(@"I click Sign up button")]
        public void WhenIClickSignUpButton()
        {
            Registration.ClickSignUpButton();
            try
            {
                Registration.GetTextError().Equals("There was an error with your registration. Please try registering again.");

                Thread.Sleep(200000);
                Registration.ClickSignUpButton();
            }
            catch (Exception e)
            {
                throw new Exception("no Element...", e);
            }
        }

        [When(@"I enter Conformation code")]
        public void WhenIEnterConformationCode()
        {
            Registration.SetCode(methods.GetObjectMail());
        }

        [Then(@"User's personal account created")]
        public void ThenUserSPersonalAccountCreated()
        {
            Assert.IsTrue(Registration.ClickOk());
        }

        [Then(@"The message appears: 'We Need More Information'")]
        public void ThenTheMessageAppears()
        {
            string excpectedMessage = "We Need More Information";
            ChromeBrowser.GetDriver().Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(90);
            var actualMessage = Registration.GetTittleWeNeedMoreInformation();
            Assert.AreEqual(excpectedMessage, actualMessage);

        }

        [Then(@"User on the home page")]
        public void ThenUserSGoToPersonalAccountPage()
        {
            var excpectedName = _firstName + " " + _lastName;
            var actualName = Registration.GetAccountName();
            Assert.AreEqual(excpectedName, actualName);
            ChromeBrowser.CloseDriver();
        }

        [Then(@"Then The notification '(.*)' is displayed")]
        public void ThenThenTheNotificationIsDisplayed(string p0)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
