﻿using System;
using TechTalk.SpecFlow;

namespace Facebook.Tests
{
    [Binding]
    public class AuthorizationSteps
    {
        [Given(@"The page of authorization is open")]
        public void GivenThePageOfAuthorizationIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I fill Autorization form fields with Email and Password")]
        public void WhenIFillAutorizationFormFieldsWithEmailAndPassword(Table table)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click on Log In button")]
        public void WhenIClickOnLogInButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click on the button Create New Account")]
        public void WhenIClickOnTheButtonCreateNewAccount()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click on the button Forgot Password\?")]
        public void WhenIClickOnTheButtonForgotPassword()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I fill Authorization field")]
        public void WhenIFillAuthorizationField(Table table)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I fill Autorization form fields with valid data")]
        public void WhenIFillAutorizationFormFieldsWithValidData(Table table)
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The user's personal account page is open")]
        public void ThenTheUserSPersonalAccountPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The user sees the registration popup")]
        public void ThenTheUserSeesTheRegistrationPopup()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The user went to the account search page")]
        public void ThenTheUserWentToTheAccountSearchPage()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The user sees the message Wrong Credentials Invalid username or password on the display")]
        public void ThenTheUserSeesTheMessageWrongCredentialsInvalidUsernameOrPasswordOnTheDisplay()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User stay on Autorization page")]
        public void ThenUserStayOnAutorizationPage()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
