﻿using Facebook.Support;
using Facebook.TempMail;
using Facebook.UI.POM.Locators;
using Facebook.UI.POM.Method;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facebook.UI.Tests.Methods
{
    public class RegistrationMethods
    {
        Mail mail;
        public string CreateTempMail()
        {
            mail = new Mail();
            return mail.TempMail;
        }

        public Mail GetObjectMail()
        {
            return mail;
        }
        public static void ChangeLang()
        {
            if (!Registration.GetLanguage().Equals("English (US)"))
            {
                if (Registration.GetLanguage2().Equals("English (US)"))
                {
                    Registration.ClickLanguage2();
                }
                else if (Registration.GetLanguage3().Equals("English (US)"))
                {
                    Registration.ClickLanguage3();
                }
            }
        }
        public static void FillInTheRegistrationFieldsWithoutEmail(string firstName, string lastName, string newPass, int day, string month, int year, string gender)
        {
            Registration.SetFirstName(firstName);
            Registration.SetLastName(lastName);
            Registration.SetPassword(newPass);
            Registration.SetAndClickAllDate(day, month, year);

            if (gender.Equals("Male"))
                Registration.ClickMaleGender();
            else if (gender.Equals("Female"))
                Registration.ClickFemaleGender();
        }
        public static void FillInTheRegistrationFieldsWithMyGender(string firstName, string lastName, string newPass, int day, string month, int year, string selectPronoun, string genderOptional)
        {
            Registration.SetFirstName(firstName);
            Registration.SetLastName(lastName);
            Registration.SetPassword(newPass);

            Registration.ClickCustomGender();

            Registration.ClickFieldPreferredPronoun();
            Registration.SetPreferredPronoun(selectPronoun[0]);

            Registration.SetAndClickAllDateAndGender(day, month, year, genderOptional);

            //Registration.SetAndClickBirthdayDay(day);
            //Registration.SetAndClickBirthdayMonth(month);
            //Registration.SetAndClickBirthdayYear(year);

            //Registration.ClickFieldPreferredPronoun();
            //Registration.SetPreferredPronoun(selectPronoun[0]);
            //Registration.ClickFieldGenderOptional(genderOptional);
        }
    }
}
