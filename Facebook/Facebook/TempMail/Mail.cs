﻿using Facebook.POM.Method;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facebook.TempMail
{
    public class Mail
    {
        public string TempMail { get; set; }        
        public Mail()
        {
            //MailPOM.ClickRemoveBtn();
            TempMail = MailPOM.GetTempEmail();            
        }
    }
}
