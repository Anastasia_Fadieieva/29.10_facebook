﻿using Newtonsoft.Json;
using System.Net;

namespace Facebook.TempMail
{
    internal class MessageTempMail
    {
        internal string GetMessage(Mail mail)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://cryptogmail.com/api/emails?inbox="+ mail.TempMail);
            request.Method = "Get";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            var model = JsonConvert.DeserializeObject<Rootobject>(myResponse);
            var subject = model.data[0].subject;
            var code = subject.Substring(0, 6);
            return code;
        }
    }
}
