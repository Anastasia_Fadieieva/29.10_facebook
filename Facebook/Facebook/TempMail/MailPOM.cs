﻿using Facebook.Support;
using OpenQA.Selenium;

namespace Facebook.POM.Method
{
    public static class MailPOM
    {
        internal static By _tempEmail = By.CssSelector("div.temp-mail__field.temp-mail--container-shadow > div");
       
        internal static By _removeOldEmail = By.CssSelector(" div.temp-mail__field.buttons-wrapper > a.temp-mail__button.button.button--remove");

        internal static string GetTempEmail() => ChromeBrowser._driver.FindElement(_tempEmail).Text;
        internal static void ClickRemoveBtn() =>
            ChromeBrowser.GetDriver().FindElement(_removeOldEmail).Click();
    }
}
