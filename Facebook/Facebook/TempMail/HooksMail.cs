﻿using Facebook.Support;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facebook.TempMail
{
    class HooksMail
    {
        [SetUp]
        public void SetUp()
        {
            ChromeBrowser.GetDriver().Navigate().GoToUrl("https://cryptogmail.com/");
        }

        [TearDown]
        public void TearDown()
        {
            ChromeBrowser.CleanDriver();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            ChromeBrowser.CloseDriver();
        }
    }
}
